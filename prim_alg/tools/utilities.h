#ifndef __utilities_h
#define __utilities_h

#include "../graphs/graphs.h"

int convert_string_to_int(char *s);
double convert_string_to_double(char *s);
void print_graph(t_graph *g);

#endif
