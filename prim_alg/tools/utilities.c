#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

#include "utilities.h"
#include "../graphs/graphs.h"

/* Converts from string to int 
 * Used to parse user input configs
 * */
int convert_string_to_int(char *s) {
	long i = strtol(s, NULL, 10);
	if (i == LONG_MAX || i == LONG_MIN) {
		fprintf(stderr, "Error converting with strtol\n");
		exit(EXIT_FAILURE);		
	}
	
	return (int)i;
}

/* Converts from string to double 
 * Used to parse user input configs
 * */
double convert_string_to_double(char *s) {
	double d;
	sscanf(s, "%lf", &d);
	return d;
}

/*Used to print the graph representation*/
void print_graph(t_graph *g) {
	int i = 0;
	int nodes = g->n;
	int edges = g->m;
	
	printf("Node: %d\nEdges: %d\nMax weight: %f\n", g->n, g->m, g->w);

	printf("Nodes indexes: ");	
	while (i<g->n) {
		printf("%d ", g->indexes[i]);
		if (i != nodes-1) {
			printf("- ");
		}
		i++;
	}
	i=0;
	printf("\nAdjacency List: ");	
	while (i<2*edges) {
		printf("%d ", g->adj_list[i]);
		if (i != 2*edges-1) {
			printf("- ");
		}
		i++;
	}
	i=0;
	printf("\nWeights List: ");	
	while (i<2*edges) {
		printf("%f ", g->weights[i]);
		if (i != 2*edges-1) {
			printf("- ");
		}
		i++;
	}
	printf("\n");
}
