#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "read_configs.h"
#include "utilities.h"

/*Number of configs to be read*/
#define CONFIGS_NUMBER 4
/*Max number of char to be read in one called to fgets*/
#define MAX_CHAR 255

/*configs.file path*/
const char *path = "./configs/configs.file";

/*Split the string to return the parameter value*/
double read_value(char *s) {
	double value = 0;
	char *token;
	const char *delim = "=";
	
	/*Getting the second word of the string*/
	token = strtok(s, delim);
	token = strtok(NULL, delim);
	
	/*if there is a secondo word (that is, token != NULL)*/
	if (token != NULL) {
		value = convert_string_to_double(token);
	}
	return value;
}

/*Read configs.file*/
double* read_configs() {
	double *configs;
	FILE *file = fopen(path, "r");
	char tmp[MAX_CHAR];
	int cont = 0;
	
	if ((configs = malloc(sizeof(double)*CONFIGS_NUMBER)) == NULL) {
		fprintf(stderr, "Error in malloc - read_configs()");
		exit(EXIT_FAILURE);
	}	
	
	if (file != NULL) {
		while(cont < CONFIGS_NUMBER && (fgets(tmp, MAX_CHAR, file) != NULL)) {
			configs[cont] = read_value(tmp);
			cont++;
		}
	}
	else {
		fprintf(stderr, "Error in fopen - read_configs()");
		exit(EXIT_FAILURE);
	}
	if (fclose(file) == EOF) {
		fprintf(stderr, "Error in fclose - read_configs()");
	}
	
	return configs;
}
