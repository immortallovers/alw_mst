#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>

#include "graphs/graphs.h"
#include "tools/utilities.h"
#include "tools/read_configs.h"
#include "d_heap/d_heap.h"
#include "prim/prim.h"

/*Number of configs to be read*/
#define CONFIGS_NUMBER 3
#define D_HEAP_SIZE 4

const char *path_graph = "graphs/sample.graph";

/*0 < epsilon <1/2*/
double epsilon;
/*Max weight for edges*/
int w;
/*Number of nodes*/
int n;
/*NUmber of edges*/
int m;
/*Struct representing the graph*/
t_graph *g;

int main(void) {
	double *configs = read_configs();
	struct timeval start, end;
	
	if ((epsilon=configs[0]) < 0 || epsilon >= 0.5) {
		fprintf(stderr, "Epsilon must be 0 < epsilon < 1/2\n");
		exit(EXIT_FAILURE);
	}
	w = (int) configs[1];
	n = (int) configs[2];
	m = (int) configs[3];
	
	if (w == 0 || m == 0) {
		fprintf(stderr, "w and n must be > 0\n");
		exit(EXIT_FAILURE);
	}
	free(configs);
	
	g = init_graph(n, m, w);
	init_adj_list(g);
	read_graph_from_file(g, path_graph);
	//print_graph(g);
	
	t_d_heap *heap = init_d_heap(D_HEAP_SIZE);
	srand(time(NULL));
	int initial_node = rand()%n; 
	printf("Initial node: %d\n", initial_node);
	t_mst *current_mst = init_mst(g, heap, initial_node);
	gettimeofday(&start, NULL);
	prim_alg(current_mst);
	gettimeofday(&end, NULL);
	printf("Visited  Nodes: %d\n", current_mst->visited_nodes_num);
	printf("MST Weight: %lf\n", current_mst->mst_weight);
	printf("Running Time %ld us\n", ((end.tv_sec*1000000 + end.tv_usec) - (start.tv_sec*1000000 + start.tv_usec)));
	
	/*
	t_node *array[4];
	array[0] = insert_node(heap, 2, 5, 0.6);
	array[1] = insert_node(heap, 2, 5, 0.5);
	decrease_key(heap, array[0], 0.3);
	decrease_key(heap, array[1], 0.2);
	array[2] = insert_node(heap, 3, 4, 0.8);
	array[3] = insert_node(heap, 8, 9, 0.1);
	for (int i=0; i<4; i++) {
		printf("PRIO %lf, HEAP_INDEX %d, NODE_INDEX %d\n", array[i]->priority, array[i]->heap_index, array[i]->node_index);
	}
	*/
	
	exit(EXIT_SUCCESS);
}
