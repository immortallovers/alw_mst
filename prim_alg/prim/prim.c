#include <stdio.h>
#include <stdlib.h>

#include "prim.h"
#include "../graphs/graphs.h"
#include "../d_heap/d_heap.h"

/* Initializing the mst struct.
 * initial_node_index is the node used to start the search
 * */
t_mst* init_mst(t_graph *g, t_d_heap *h, int initial_node_index) {
	t_mst *current_mst;
	t_node **current_nodes;
	t_node *first_node;
	int *v_nodes_flag;
	int i;
	
	if ((current_mst = malloc(sizeof(t_mst))) == NULL) {
		fprintf(stderr, "Error in malloc - init_mst() - mst\n");
		exit(EXIT_FAILURE);
	}
	
	/*Allocating a reference for each node*/
	if ((current_nodes = malloc(sizeof(t_node*)*g->n)) == NULL) {
		fprintf(stderr, "Error in malloc - init_d_heap() - visited_nodes\n");
		exit(EXIT_FAILURE);
	}
	/*Initializing each reference to NULL*/
	for (i=0; i<g->n; i++) {
		current_nodes[i] = NULL;
	}
	current_mst->nodes_ref = current_nodes;
	current_mst->graph = g;
	
	if ((v_nodes_flag = malloc(sizeof(int)*g->n)) == NULL) {
		fprintf(stderr, "Error in malloc - init_d_heap() - visited_nodes_flags\n");
		exit(EXIT_FAILURE);
	}
	/*Initializing each flag to false*/
	for (i=0; i<g->n; i++) {
		v_nodes_flag[i] = 0;
	}
	current_mst->visited_nodes_flag = v_nodes_flag;	
	
	/*Putting the initial node in the heap with priority 0 and edge 0*/
	first_node = insert_node(h, initial_node_index, 0, 0.0);
	current_mst->nodes_ref[initial_node_index] = first_node;
	current_mst->heap = h;
	
	current_mst->visited_nodes_num = 0;
	current_mst->mst_weight = 0.0;
	
	return  current_mst;
}

/*Updates the priority queue with the new reachable nodes*/
void update_neighbours(t_mst *current_mst, int node_index) {
	int begin, end;
	int neighbour_node;
	
	begin = current_mst->graph->indexes[node_index];
	
	/*The end index depends on the node_index. If the node_index is the last node in the graph, then end is the end of the adj_list*/
	if (node_index <= current_mst->graph->n-2) {
		end = current_mst->graph->indexes[node_index+1];
	}
	else {
		end = (current_mst->graph->m)*2-1;
	}
	
	for (int i=begin; i<end; i++) {
		/*neighbour_node might be -1 if there are self loops (there are less then 2*m edges in the adj_list)*/
		neighbour_node = current_mst->graph->adj_list[i];
		if (neighbour_node != -1 && !current_mst->visited_nodes_flag[neighbour_node]) {
			/*This node was not visited yet*/
			if(current_mst->nodes_ref[neighbour_node] == NULL) {
				/*The node was not yet inserted in the heap*/
				current_mst->nodes_ref[neighbour_node] = insert_node(current_mst->heap, neighbour_node, i, current_mst->graph->weights[i]);
			}
			else if (current_mst->nodes_ref[neighbour_node]->priority > current_mst->graph->weights[i]){
				/*The node is in the heap but we need to update its priority*/
				decrease_key(current_mst->heap, current_mst->nodes_ref[neighbour_node], current_mst->graph->weights[i], i);				
			}
		}
	}
}

/*Main function - implements the Prim's algorithm*/
void prim_alg(t_mst *current_mst) {
	t_node *current;
	int node_index;
	double new_weight = 0.0;
	
	while (!is_empty(current_mst->heap)) {
		/*There are new nodes to visit*/
		/*Deleting the root of the d-heap*/
		current = find_min(current_mst->heap);
		node_index = current->node_index;
		new_weight = delete_min(current_mst->heap);
		current_mst->nodes_ref[node_index] = NULL;
		current_mst->mst_weight += new_weight;
		//printf("Visited node = %d, New weight = %lf, MST weight = %lf\n", node_index, new_weight, current_mst->mst_weight);
		current_mst->visited_nodes_flag[node_index] = 1;
		++current_mst->visited_nodes_num;
		/*Updating the priority queue with the new neighbours*/
		update_neighbours(current_mst, node_index);
	}
}
