#ifndef __prim_h
#define __prim_h

#include "../graphs/graphs.h"
#include "../d_heap/d_heap.h"

typedef struct mst {
	t_graph *graph; 
	t_d_heap *heap;
	/* Array of references to the nodes in the d-Heap struct (nodes yet to be visited but reachable from the current MST)
	 * Each node's index in the array is the index of the same node in the graph*
	 * Used to retrieve the node when it sould be updated (decrease_key) */
	t_node **nodes_ref;
	/*Array of flags. If visited_nodes_flag[i] == 1, the i node was visited (belongs to the mst)*/
	int *visited_nodes_flag;
	/*Current number of visited nodes*/
	int visited_nodes_num;
	/*The weight of the current MST*/
	double mst_weight;	
} t_mst;

t_mst* init_mst(t_graph *g, t_d_heap *h, int initial_node_index);
void prim_alg(t_mst *current_mst);
void update_neighbours(t_mst *current_mst, int node_index);

#endif 
