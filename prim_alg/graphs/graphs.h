#ifndef __graphs_h
#define __graphs_h

/*The graph is rappresented as CSR format*/
typedef struct graph {
	double w;
	int n;
	int m;
	/*for node i, returns the start index of adj_list to read neighbours*/
	int *indexes;
	int *adj_list;
	double *weights;	
} t_graph;

t_graph* init_graph(int nodes, int edges, double max_weight);
void init_adj_list(t_graph *g);
void read_graph_from_file(t_graph *g, const char *file_name);

int get_degree(int node);

#endif
