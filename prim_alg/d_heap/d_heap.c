#include <stdio.h>
#include <stdlib.h>

#include "d_heap.h"
#include "../graphs/graphs.h"


/*Initialize the d-Heap*/
t_d_heap* init_d_heap(int degree) {
	t_d_heap *heap;
	
	if ((heap = malloc(sizeof(t_d_heap))) == NULL) {
		fprintf(stderr, "Error in malloc - init_d_heap() - heap\n");
		exit(EXIT_FAILURE);
	}
	
	/*Allocating the two initial levels of the tree (parent + d children)*/
	if ((heap->nodes = malloc(sizeof(t_node*)*(degree+1))) == NULL) {
		fprintf(stderr, "Error in malloc - init_d_heap() - nodes\n");
		exit(EXIT_FAILURE);
	}
	
	heap->allocated_mem = degree+1;
	heap->num_nodes = 0;
	heap->d = degree;
	
	return heap;
}

/*Returns 1 if the heap has 0 nodes, 0 otherwise*/
int is_empty(t_d_heap *heap) {
	if (heap->num_nodes == 0) {
		return 1;
	}
	return 0;
}

/*Returns the edge with the min weight (the parent of the tree)*/
t_node* find_min(t_d_heap *heap) {
	if (is_empty(heap)) {
		return NULL;
	}
	return *(heap->nodes);
}

/*Returns the index of the parent node*/
int get_parent_index(int node_index, int degree) {
	if (node_index != 0) {
		/*The node is not the tree root*/
		return (node_index-1)/degree;
	}
	return 0;
}

/*Switch the given nodes*/
int switch_nodes(t_node **nodes, int node1_index, int node2_index) {
	nodes[node1_index]->heap_index = node2_index;
	nodes[node2_index]->heap_index = node1_index;
	t_node *tmp = nodes[node1_index];
	nodes[node1_index] = nodes[node2_index];
	nodes[node2_index] = tmp;
	/*When switch child with parent, return the new parent/child*/
	return node2_index;
}

/* Push the new node, or the updated node, up to its right position*/
void push_node_up(t_d_heap *heap, int node_index) {
	t_node **array = heap->nodes;
	int parent_index = get_parent_index(node_index, heap->d);
	
	while (array[node_index]->priority < array[parent_index]->priority) {
		/*The parent has lower priority (higher weight)*/
		node_index = switch_nodes(heap->nodes, node_index, parent_index);
		parent_index = get_parent_index(node_index, heap->d);
	}
	
}

/*Returns the index of the child with min wight*/
int get_min_child(t_d_heap *heap, int node_index) {
	int i = 1;
	int child_index = (node_index * heap->d) +i;
	int min_index;
	double min;
	t_node **array = heap->nodes;
	
	if (child_index > heap->num_nodes-1) {
		/*The node doesn't have children - we're in the lowest level of the tree*/
		return -1;
	}
	
	min = array[child_index]->priority;
	min_index = child_index;
	i++;
	child_index++;
	while (i<=heap->d && child_index < heap->num_nodes) {
		/*No more than d children for each node*/
		if (array[child_index]->priority < min) {
			min = array[child_index]->priority;
			min_index = child_index;
		}
		i++;
		child_index++;
	}
	return min_index;
}

/*Push the root down after delete_min*/
void push_node_down(t_d_heap *heap) {
	t_node **array = heap->nodes;
	/*Getting the root's child with the min weight*/
	int min_child_index = get_min_child(heap, 0);
	int node_index = 0;
	
	while ((min_child_index >= 0) && array[node_index]->priority > array[min_child_index]->priority) {
		node_index = switch_nodes(heap->nodes, node_index, min_child_index);
		min_child_index = get_min_child(heap, node_index);
	}
}

/* Creates a new node with the given arguments, appends it to 
 * the end of the tree and then push it up to the right position*/
t_node* insert_node(t_d_heap *heap, int n_index, int e_index, double pri) {
	
	t_node *new_node;
	t_node **array;
	
	/*First we need to check that there is enough space to append the new node*/
	if (heap->num_nodes == heap->allocated_mem) {
		/*We need to allocate new space*/
		heap->allocated_mem *= 2;
		if ((heap->nodes = realloc(heap->nodes, sizeof(t_node*)*heap->allocated_mem)) == NULL) {
			fprintf(stderr, "Error in realloc - insert_node()\n");
			exit(EXIT_FAILURE);
		}
	}
	
	/*Creating the new node*/
	if ((new_node = malloc(sizeof(t_node))) == NULL) {
		fprintf(stderr, "Error in malloc - insert_node()\n");
		exit(EXIT_FAILURE);
	}
	new_node->node_index = n_index;
	new_node->edge_index = e_index;
	new_node->priority = pri;
	new_node->heap_index = heap->num_nodes;
	
	array = heap->nodes;
	array[heap->num_nodes] = new_node;
	heap->num_nodes++;
	push_node_up(heap, heap->num_nodes-1);
	return new_node;
}

/*Deletes the node at the root of the tree and returns the min weight*/
double delete_min(t_d_heap *heap) {
	double min_weight;
	t_node **array = heap->nodes;
	
	if (is_empty(heap)) {
		printf("VOID HEAP - No elements to delete\n");
		return -1;
	}
	
	min_weight = array[0]->priority;
	if (heap->num_nodes == 1) {
		free(array[0]);
		heap->num_nodes -= 1;
	}
	else {
		/*Switching the root with last one (right most leaf)*/
		switch_nodes(heap->nodes, 0, heap->num_nodes-1);
		free(array[heap->num_nodes-1]);
		heap->num_nodes -= 1;
		push_node_down(heap);
	}
	
	/*
	if (heap->num_nodes == heap->allocated_mem/4) {
		heap->allocated_mem /= 2;
		if ((heap->nodes = realloc(heap->nodes, sizeof(t_node*)*heap->allocated_mem)) == NULL) {
			fprintf(stderr, "Error in realloc - delete_min()\n");
			exit(EXIT_FAILURE);
		}
	}
	*/
	 
	return min_weight;
}

void decrease_key(t_d_heap *heap, t_node *update_node, double new_priority, int new_edge_index) {
	if (update_node->priority < new_priority) {
		/*The current weight is lower the new_priority*/
		printf("No need to update priority\n");
		return;
	}
	update_node->priority = new_priority;
	update_node->edge_index = new_edge_index;
	push_node_up(heap, update_node->heap_index);	
}
