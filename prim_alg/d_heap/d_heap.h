#ifndef __d_heap_h
#define __d_heap_h

/*Element of the d-Heap*/
typedef struct node {
	int node_index;
	/*Index of the edge with min weight of the node in the grap adj_list (has the same index in the weights list)*/
	int edge_index;
	/*The weight of the selected edge*/
	double priority;
	/*Index of the node in the heap*/
	int heap_index;
} t_node;

/*d-Heap represented as a vector*/
typedef struct d_heap {
	t_node **nodes;
	int d;
	/*Current number of allocated nodes (might be less tha allocated_mem)*/
	int num_nodes;
	/*number of nodes that can me allocated on the array*/
	int allocated_mem;	
} t_d_heap;

t_d_heap* init_d_heap(int degree);
int is_empty(t_d_heap *heap);
t_node* find_min(t_d_heap *heap);
t_node* insert_node(t_d_heap *heap, int n_index, int e_index, double pri);
void push_node_up(t_d_heap *heap, int node_index);
void push_node_down(t_d_heap *heap);
int get_min_child(t_d_heap *heap, int node_index);
int get_parent_index(int node_index, int degree);
int switch_nodes(t_node **nodes, int node1_index, int node2_index);
double delete_min(t_d_heap *heap);
void decrease_key(t_d_heap *heap, t_node *update_node, double new_priority, int new_edge_index);

#endif
