# README #

### What is this repository for? ###

* Implementing and testing an algorithm approximating the MST weight in sublinear time.
* [Paper Reference](http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.443.7744&rep=rep1&type=pdf)

### How do I get set up? ###

* make
* make debug (debugging with GDB)