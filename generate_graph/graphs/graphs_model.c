#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "graphs_model.h"

/*Initializing the struct t_tmp_info*/
void init_tmp_info(t_tmp_info *tmp, int m) {
	
	if ((tmp->tmp_edges = malloc(sizeof(int)*m*2)) == NULL) {
		fprintf(stderr, "Error in malloc - init_tmp_info\n");
		exit(EXIT_FAILURE);
	}
	
	tmp->cont_edges = m;
}


/* Creating a random connected graph
 * tmp_edges containing the edges in the form (0,1) (2,3) ... (i, i+1)
 * */
void random_connected_graph(t_tmp_info *tmp, int nodes, int edges) { 
	/*Used to create the first connected component of the graph*/
	char connected_nodes[nodes];
	/*Current number of created edges*/
	int current_edges = 0;
	int i = 0;
	int j = 0;
	int node_to, node_from;
	srand(time(NULL));
	
	/*Initializing the tmp struct*/
	init_tmp_info(tmp, edges);	
	
	/*Setting the array connected_nodes to false*/
	memset(connected_nodes, 0, sizeof(char)*nodes);
	
	/*To have a connected graph, m >= n-1*/
	/*Creating the first edge from node 0 to a second random distinct node*/
	while((node_to = rand()%nodes) == i);
	tmp->tmp_edges[j] = i;
	tmp->tmp_edges[j+1] = node_to;
	connected_nodes[i] = 1;
	connected_nodes[node_to] = 1;
	current_edges++;
	i++;
	j=2;
	while (current_edges<nodes-1 && i<nodes) {
		/*Generating at least one edge for each node (we want a connected graph)*/
		if(!connected_nodes[i]) {
			/*node i doesn't belong to the connected component*/
			while ((node_to = rand()%nodes) == i || connected_nodes[node_to] != 1);
			tmp->tmp_edges[j] = i;
			tmp->tmp_edges[j+1] = node_to;
			connected_nodes[i] = 1;
			current_edges++;
			j+=2;
		}
		i++;
	}
	
	if (edges>nodes-1) {
		while (current_edges<edges) {
			/*Generating the edges by choosing the adjacent nodes at random - might be self loops*/
			node_from = rand()%nodes;
			node_to = rand()%nodes;
			tmp->tmp_edges[j] = node_from;
			tmp->tmp_edges[j+1] = node_to;
			current_edges++;
			j+=2;
		}
	}
	
	/*
	for (j=0; j<2*edges; j+=2) {
		printf("(%d, %d)", tmp_edges[j], tmp_edges[j+1]);
	}
	printf("\n");
	*/
}

/* Creating a completely random graph (Erdos-Renyi model G(n,m))
 * tmp_edges containing the edges in the form (0,1) (2,3) ... (i, i+1)
 * */
void random_graph(t_tmp_info *tmp, int nodes, int edges) { 
	srand(time(NULL));
	int j = 0;
	int node_to, node_from;
	
	/*Initializing the tmp struct*/
	init_tmp_info(tmp, edges);	
	
	for(int i=0; i<edges; i++) {
		node_from = rand()%nodes;
		node_to = rand()%nodes;
		tmp->tmp_edges[j] = node_from;
		tmp->tmp_edges[j+1] = node_to;
		j+=2;
	}
	
	/*for (j=0; j<2*edges; j+=2) {
		printf("(%d, %d)", tmp_edges[j], tmp_edges[j+1]);
	}
	printf("\n");*/
}

/* Creating a Erdos-Renyi model graph G(n,p)
 * tmp_edges containing the edges in the form (0,1) (2,3) ... (i, i+1)
 * The degrees follow a binomial distribution so
 * d=np ~ 2*edges/nodes
 * e=bin(n,2)p ~ nodes*d/2
 * */
void erdos_renyi_graph(t_tmp_info *tmp, int nodes, int d) {
	double p;
	int i,j, x=0;
	int cont_edges = 0;
	srand(time(NULL));
	int edges;
		
	/*Initializing the tmp struct*/
	edges = (d/2)*nodes;
	init_tmp_info(tmp, edges);		
		
	p = (double)d/(nodes*2);
	printf("Edges %d - p %lf\n", edges,p);
		
	for(i=0; i<nodes; i++) {
		for (j=0; j<nodes; j++) {
			if (rand()/(double)RAND_MAX < p) {
				cont_edges++;
				if (cont_edges==edges) {
					if ((tmp->tmp_edges = realloc(tmp->tmp_edges, sizeof(int)*2*(cont_edges+1))) == NULL) {
						fprintf(stderr, "Error in realloc - erdos_renyi_graph()");
						exit(EXIT_FAILURE);
					}
					edges++;
				}
				tmp->tmp_edges[x] = i;
				tmp->tmp_edges[x+1] = j;
				x += 2;
			}
		}
	}
	tmp->cont_edges = cont_edges;
	printf("Cont_edges = %d\n", cont_edges);
}

/* Creating a Watts-Strogatz model graph (small world)
 * tmp_edges containing the edges in the form (0,1) (2,3) ... (i, i+1)
 * */
void watts_strogatz_graph(t_tmp_info *tmp, int nodes, int k) {
	double p = 0.3;
	/* @k = initial number of edges for each node; used to create the ring lattice
	 * @half_k = k/2 (k and k/2 must be even) */
	int half_k;
	int cont_edges = 0;
	int cont_half_k, neighbour;
	int node_to;
	int edges;
	int i,j;
	
	half_k = k/2;
	printf("K = %d - K/2 = %d\n", k, half_k);
	
	/*Initializing the tmp struct*/
	edges = nodes*half_k;
	tmp->cont_edges = edges;
	printf("Edges %d\n", edges);
	init_tmp_info(tmp, edges);		
	
	/*Creating the ring lattice*/
	j=0;
	/*First we need to create the edges building the ring*/
	for(i=0; i<nodes-1; i++) {
		tmp->tmp_edges[j] = i;
		tmp->tmp_edges[j+1] = i+1;
		j+=2;
		cont_edges++;
	}
	/*Last edge in the ring*/
	tmp->tmp_edges[j] = i;
	tmp->tmp_edges[j+1] = 0;
	cont_edges++;
	j+=2;
	
	
	/*Then we need to create the lattice*/
	for(i=0; i<nodes; i++) {
		cont_half_k = 2;
		while (cont_half_k<=half_k) {
			tmp->tmp_edges[j] = i;
			neighbour = i+cont_half_k;
			if(neighbour>=nodes) {
				neighbour = neighbour-nodes;
			}
			tmp->tmp_edges[j+1] = neighbour;
			cont_half_k++;
			cont_edges++;
			j+=2;
		}
	}
	
	if (cont_edges != edges) {
		printf("Error in ring lattice creation - cont_edges = %d\n", cont_edges);
	}	
	
	/*Rewiring the edges with probability p*/
	for (j=0; j<2*edges; j+=2) {
		if ((rand()/(double)RAND_MAX) < p) {
			node_to = rand()%nodes;
			tmp->tmp_edges[j+1] = node_to;
		}
	}
	/*
	for (j=0; j<2*edges; j+=2) {
		printf("(%d, %d)", tmp->tmp_edges[j], tmp->tmp_edges[j+1]);
	}
	printf("\n");
	* */
}

/* Creating an Albert-Barabasi model graph (scale free)
 * @m0 = number of nodes in the initial complete graph
 * @d = average degree
 * @m = number of edges for each new nodes (there might be multiple edges)
 * nodes = m0+t
 * edges = mt+m0
 * d = 2m;
 * p = degree_i/sum_j(degree_j) -> the probability to be attached to a node i 
 * is (degree of i)/(degree of all nodes in the graph) that is (degree_i)/(2*current_edges)
 * tmp_edges containing the edges in the form (0,1) (2,3) ... (i, i+1)
 * */
void albert_barabasi_graph(t_tmp_info *tmp, int nodes, int m0, int d) {
	int current_nodes_degrees[nodes];
	int m;
	int i,j,pos_array;
	int cont, current_num_edges;
	int extracted_node;
	//double p;
	/*The number of edges is m*(nodes-m0) + binomial_coefficient(m0,2)*/
	int edges, bin_coefficient=1;
	srand(time(NULL));
	
	/*Initializing values and array*/
	m = d/2;
	current_num_edges = 0;
	pos_array = 0;
	for (i=0; i<nodes;  i++) {
		current_nodes_degrees[i] = 0;
	}
	
	/*Initializing the tmp struct*/
	bin_coefficient = m0*(m0-1)/2;
	edges = m*(nodes-m0) + bin_coefficient;
	init_tmp_info(tmp, edges);	
	printf("Edges %d\n", tmp->cont_edges);
	
	/*Creating the initial complete graph*/
	for (i=0; i<m0; i++) {
		for (j=i+1; j<m0; j++) {
			tmp->tmp_edges[pos_array] = i;
			tmp->tmp_edges[pos_array+1] = j;
			current_num_edges++;
			//current_nodes_degrees[i] += 1;
			//current_nodes_degrees[j] += 1;
			pos_array+=2;
		}
	}

	if (current_num_edges != bin_coefficient) {
		printf("Error creating the complete grap - current_edges = %d\n", current_num_edges);
	}
	
	/*Adding the remaining nodes*/
	for (i=m0; i<nodes; i++) {
		cont = 0;
		while (cont<m) {
			/*Choosing the m edges*/
			
			/* ALGORITHM AS DESCRIBED FROM ALBERT-BARABASI PAPER
			extracted_node = rand()%i;
			p = ((double)current_nodes_degrees[extracted_node])/(2*current_num_edges);
			if (rand()/(double)RAND_MAX < p) {
				tmp->tmp_edges[pos_array] = i;
				tmp->tmp_edges[pos_array+1] = extracted_node;
				current_nodes_degrees[i] += 1;
				current_nodes_degrees[extracted_node] += 1;
				pos_array+=2;
				current_num_edges++;
				cont++;
			}*/
			
			/* ALGORITHM DESCRIBED IN "Efficient generation of large Random Networks" Batagelj and Brandes
			 * Optimization of the algorithm - we're using the array of already selected edges 
			 * to extract the neighbour (this node will be in the array a number of times equal 
			 * to its degree so we have the same probability as before of a node to be extracted)*/
			extracted_node = tmp->tmp_edges[rand()%pos_array];
			tmp->tmp_edges[pos_array] = i;
			tmp->tmp_edges[pos_array+1] = extracted_node;
			current_nodes_degrees[i] += 1;
			current_nodes_degrees[extracted_node] += 1;
			pos_array+=2;
			current_num_edges++;
			cont++;
		}
	}
	/*
	printf("Current_edges %d\n", current_num_edges);
	for(j=0; j<nodes; j++) {
		printf("%d, %d\n", j, current_nodes_degrees[j]);
	}
	for (j=0; j<2*tmp->cont_edges; j+=2) {
		printf("(%d, %d)", tmp->tmp_edges[j], tmp->tmp_edges[j+1]);
	}
	printf("\n");
	* */
}
