#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "graphs.h"
#include "graphs_model.h"
#include "../tools/utilities.h"

#define RAND_CON        0
#define RAND_ER         1
#define ERDOS_RENYI     2
#define WATTS_STROGATZ  3
#define ALBERT_BARABASI 4

/*Creates and allocates the graph struct*/
t_graph* init_graph(int nodes, int edges, double max_weight) {
	t_graph *g;
	
	if ((g = malloc(sizeof(t_graph))) == NULL) {
		fprintf(stderr, "Error in malloc - init_graph()");
		exit(EXIT_FAILURE);
	}
	g->w = max_weight;
	g->n = nodes;
	g->m = edges;
	
	if ((g->indexes = malloc(sizeof(int)*nodes)) == NULL) {
		fprintf(stderr, "Error in malloc - init_graph() - indexes");
		exit(EXIT_FAILURE);
	}
	
	/* The init of the adj_list and weights arrays is postponed in order to find 
	 * the exact number of edges on the graph
	 * */
	
	return g;
}


/*Initializes the indexes array, to prepare it for the graph generation and edges insertion*/
void init_indexes_array(t_graph *g) {
	int i;
	
	for (i=0; i<g->n; i++) {
		g->indexes[i] = 0;
	}
}

/* 
 * Intizialing the adjacency list - the array is 2*m but we may have less edges due to self loops 
 * so we assign to array a value -1 (this will also help when generating the list with the indexes)
 */
void init_adj_list(t_graph *g) {
	int tot_edges = 2*g->m;
	
	/*Allocating the adj_list and the weights arrays*/

	/*Each edge is represented twice in the array*/
	if ((g->adj_list = malloc(sizeof(int)*tot_edges)) == NULL) {
		fprintf(stderr, "Error in malloc - init_graph() - adj_list");
		exit(EXIT_FAILURE);
	}
	
	/*Each edge is represented twice in the array*/
	if ((g->weights = malloc(sizeof(double)*tot_edges)) == NULL) {
		fprintf(stderr, "Error in malloc - init_graph() - weights");
		exit(EXIT_FAILURE);
	}
	
	for (int i=0; i<tot_edges; i++) {
		g->adj_list[i] = -1;
	}
}

/*
 * Inserting the edge connecting the current node to node_to
 * index_from/index_to refers to the subset of the adj_list belonging to the current node
 * */
void insert_edge(int index_from, int index_to, int node_to, t_graph *g) {
	int i = index_from;
	
	while (i<index_to) {
		if (g->adj_list[i] == -1) {
			break;
		}
		i++;
	}
	if (i<index_to) {
		g->adj_list[i] = node_to;
	}
	else {
		/*Should NEVER happen - better safe than sorry*/
		printf("Error while inserting an edge int adj_list - not enough space in the array for the node's degree\n");
		exit(EXIT_FAILURE);
	}
}

/*Generating the adjacency list using the indexes array*/
void generate_adj_list(int *edges, t_graph *g) {
	int from, to;
	int tot_nodes = g->n;
	int tot_edges = 2*g->m;
	
	for (int i=0; i<tot_edges; i+=2) {
		from = edges[i];
		to = edges[i+1];
		if (from != to) {
			/*Doing a double check because the graph is undirected so we need to count each edge twice in the adj_list*/
			if (from != tot_nodes-1) {
				/*This is not the last node*/
				insert_edge(g->indexes[from], g->indexes[from+1], to, g);
			}
			else {
				/*This is the last node*/
				insert_edge(g->indexes[from], tot_edges, to, g);
			}
			if (to != tot_nodes-1) {
				/*This is not the last node*/
				insert_edge(g->indexes[to], g->indexes[to+1], from, g);
			}
			else {
				/*This is the last node*/
				insert_edge(g->indexes[to], tot_edges, from, g);
			}
		}
		else {
			/*self loop*/
			if (from != tot_nodes-1) {
				insert_edge(g->indexes[from], g->indexes[from+1], from, g);
			}
			else {
				insert_edge(g->indexes[from], tot_edges, from, g);
			}
		}
	}
	
}

/*Generating the indexes array*/
void generate_indexes(int *edges, t_graph *g) {
	int i;
	int nodes, node1, node2;
	int tmp1, tmp2;
	int tot_edges = 2*g->m;
	
	/*First we need to count the nodes degrees and put them in the indexes array*/
	for (i=0; i<tot_edges; i+=2) {
		node1 = edges[i];
		node2 = edges[i+1];
		g->indexes[node1] += 1;
		if (node1!=node2) {
			/*Avoiding counting self loops twice*/
			g->indexes[node2] += 1;
		}
	}

	/*Then we assign the right indexes using the degrees*/
	nodes = g->n;
	tmp1 = g->indexes[0];
	g->indexes[0] = 0;
	for (i=1; i<nodes; i++) {
		tmp2 = g->indexes[i];
		g->indexes[i] = g->indexes[i-1] + tmp1;
		tmp1 = tmp2;
	}
}

/*Generating the edges' weights*/
void generate_weights(t_graph *g) {
	int max_weight = g->w;
	int tot_edges = 2*g->m;
	
	for (int i=0; i<tot_edges; i++) {
		/*-------------------
		 * Per generare pesi non interi sostituire l'assegnazione con 
		 * weight = max_weight*((double)rand()/MAX_RAND)+1
         *-------------------*/
		g->weights[i] = rand()%max_weight+1;
	}
}

/*Initializing the bfs struct*/
t_bfs* init_bfs_struct(int n, int m) {
	t_bfs *bfs_struct;
	
	if ((bfs_struct = malloc(sizeof(t_bfs))) == NULL) {
		fprintf(stderr, "Error in malloc - init_bfs_struct() - bfs_struct");
		exit(EXIT_FAILURE);
	}
	
	if ((bfs_struct->visited_nodes = malloc(sizeof(int)*n)) == NULL) {
		fprintf(stderr, "Error in malloc - init_bfs_struct() - visited_nodes");
		exit(EXIT_FAILURE);
	}
	
	for (int i=0; i<n; i++) {
		/*Initializes the array to false*/
		bfs_struct->visited_nodes[i] = 0;
	}	
	
	bfs_struct->visited_nodes_num = 0;
	bfs_struct->updated_m = m;
	
	return bfs_struct;
}


/*Adding new edges to visited nodes, to connect the graph*/ 
int* update_edges(int *tmp_edges, t_bfs *bfs_struct, int nodes, int current_m) {
	int cont = 0;
	int node_found = 0;
	int to;
	srand(time(NULL));
	
	if ((tmp_edges = realloc(tmp_edges, sizeof(int)*2*bfs_struct->updated_m)) == NULL) {
		fprintf(stderr, "Error in realloc - update_edges()\n");
		exit(EXIT_FAILURE);
	}
	
	for (int i=0; i<nodes; i++) {
		if (!bfs_struct->visited_nodes[i]) {
			/*The node was not visited during the BFS*/
			while (!node_found) {
				to = rand()%nodes;
				if (bfs_struct->visited_nodes[to]) {
					node_found = 1;
				}
			}
			tmp_edges[(current_m*2)+cont] = i;
			tmp_edges[(current_m*2)+cont+1] = to;
			bfs_struct->visited_nodes[i] = 1;
			cont += 2;
			node_found = 0;
		}
	}
	printf("CONT EDGES %d - Previous edges %d - Current Edges %d\n", cont/2, current_m, bfs_struct->updated_m);
	return tmp_edges;
}


/*Returns the degree of the given node*/
int get_degree(t_graph *g, int node_index) {
	int from, to;
	int degree = 0;
	
	from = g->indexes[node_index];
	if (node_index < g->n-1) {
		to = g->indexes[node_index+1];
	}
	else {
		to = 2*g->m;
	}
	
	for (int i=from; i<to; i++) {
		if (g->adj_list[i] != -1) {
			degree++;
		}
	}
	
	return degree;
}


/* -----------------------------------------
 * 			Breadth First Search
 * -----------------------------------------*/
void breadth_first_search(t_bfs *bfs_struct, t_graph *g) {
	/*Used like a queue to keep the open nodes*/
	int queue[g->n];
	/*Point to the current begin/end of the queue*/
	int queue_begin_index, queue_end_index;
	/*The current visited node*/
	int open_node;
	/*Indexes used to read the adj_list of the open node*/
	int from, to;
	int i;
	
	queue_begin_index = 0;
	srand(time(NULL));
	open_node = rand()%g->n;
	queue[queue_begin_index] = open_node;
	bfs_struct->visited_nodes[open_node] = 1;
	++bfs_struct->visited_nodes_num;
	queue_end_index = 1;
	
	while (queue_begin_index != queue_end_index) {
		/*There are still node to check in the queue*/
		open_node = queue[queue_begin_index];
		//printf("Open Node %d\n", open_node);
		from = g->indexes[open_node];
		if (open_node != g->n-1) {
			to = g->indexes[open_node+1];
		}
		else {
			to = (g->m)*2;
		}
		for (i=from; i<to; i++) {
			if ((g->adj_list[i]!=-1) && (!(bfs_struct->visited_nodes[g->adj_list[i]]))) {
				/*The open node's neighbour was not visited yet*/
				/*Adding the node to the queue*/
				queue[queue_end_index] = g->adj_list[i];
				queue_end_index++;
				/*Marking the node as visited*/
				bfs_struct->visited_nodes[g->adj_list[i]] = 1;
				++bfs_struct->visited_nodes_num;
			}
		}
		queue_begin_index++;
	}
	
	if (g->n != bfs_struct->visited_nodes_num)  {
		/*The graph is not connected -  we need to add some edges, one for each non visited node*/
		bfs_struct->updated_m += g->n - bfs_struct->visited_nodes_num;
	}
}


/* -----------------------------------------
 * Main function for random graph generation
 * -----------------------------------------*/
void generate_graph(t_graph *g, int d, int model) {
	int nodes = g->n;
	int edges = g->m;
	t_bfs *bfs_struct;
	/*Tmp array containing the edges in the form (0,1) (2,3) ... (i, i+1)*/
	t_tmp_info *tmp;

	if ((tmp = malloc(sizeof(t_tmp_info))) == NULL) {
		fprintf(stderr, "Error in malloc - generate_graph() - t_tmp_info");
		exit(EXIT_FAILURE);
	}
	
	/* --------------------------------------------------
	 * Generating the graph according to the chosen model
	 * --------------------------------------------------*/
	switch (model) {
		case RAND_CON:
			random_connected_graph(tmp, nodes, edges);
			break;
		case RAND_ER:
			random_graph(tmp, nodes, edges);
			break;
		case ERDOS_RENYI:
			erdos_renyi_graph(tmp, nodes, d);
			break;
		case WATTS_STROGATZ:
			watts_strogatz_graph(tmp, nodes, d);
			break;
		case ALBERT_BARABASI:
			albert_barabasi_graph(tmp, nodes, 4, d);
	}

	/*Intizialing the index array*/
	g->m = tmp->cont_edges;
	init_indexes_array(g);
	init_adj_list(g);
	
	generate_indexes(tmp->tmp_edges, g);
	generate_adj_list(tmp->tmp_edges, g);
	
	/*Starting BFS search to check if the graph is connected*/
	bfs_struct = init_bfs_struct(nodes, edges);
	breadth_first_search(bfs_struct, g);
	
	if (bfs_struct->updated_m != edges) {
		/* --------------------------------
		 * 	   	Should never be true
		 * --------------------------------*/
		/*The graph was not connected so we need to update the edges*/
		printf("The graph is NOT connected - Visisted nodes: %d\n", bfs_struct->visited_nodes_num);
		tmp->tmp_edges = update_edges(tmp->tmp_edges, bfs_struct, nodes, edges);
		
		/*Reallocating the arrays in the graph struct to add new edges*/
		if ((g->adj_list = realloc(g->adj_list, sizeof(int)*2*bfs_struct->updated_m)) == NULL) {
			fprintf(stderr, "Error in realloc - generate_graph() - adj_list\n");
			exit(EXIT_FAILURE);
		}
		if ((g->weights = realloc(g->weights, sizeof(double)*2*bfs_struct->updated_m)) == NULL) {
			fprintf(stderr, "Error in realloc - generate_graph() - weights\n");
			exit(EXIT_FAILURE);
		}
		g->m = bfs_struct->updated_m;
		
		/*
		for (j=0; j<2*bfs_struct->updated_m; j+=2) {
			printf("(%d, %d)", tmp_edges[j], tmp_edges[j+1]);
		}
		printf("\n");
		*/
		
		init_indexes_array(g);
		init_adj_list(g);
		generate_indexes(tmp->tmp_edges, g);
		generate_adj_list(tmp->tmp_edges, g);
	}
	else {
		printf("The graph is connected\n");
	}
	
	
	/*Starting BFS search to check if the graph is connected*/
	t_bfs *bfs_check = init_bfs_struct(g->n, g->m);
	breadth_first_search(bfs_check, g);
	if (bfs_check->visited_nodes_num != g->n) {
		printf("Dopo l'aggiunta il grafo NON è connesso\n");
	}
	else {
		printf("Dopo l'aggiunta il grafo È connesso - visistati %d nodi\n", g->n);
	}
	/*
	for (int j=0; j<nodes; j++) {
		printf("%d, %d\n", j, get_degree(g, j));
	}*/
	
	generate_weights(g);
}
