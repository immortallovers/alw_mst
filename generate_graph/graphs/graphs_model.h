#ifndef __graphs_model_h
#define __graphs_model_h

typedef struct tmp_info {
	/*List of edges in the form of (0,1) (2,3 ) ... (i, i+1)*/
	int* tmp_edges;
	/*Number of edges created*/
	int cont_edges;
} t_tmp_info;

void init_tmp_info(t_tmp_info *tmp, int m);

void random_connected_graph(t_tmp_info *tmp, int nodes, int edges);
void random_graph(t_tmp_info *tmp, int nodes, int edges);
void erdos_renyi_graph(t_tmp_info *tmp, int nodes, int d);
void watts_strogatz_graph(t_tmp_info *tmp, int nodes, int k);
void albert_barabasi_graph(t_tmp_info *tmp, int nodes, int m0, int d);

#endif
