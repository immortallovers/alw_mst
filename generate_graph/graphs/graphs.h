#ifndef __graphs_h
#define __graphs_h

/*The graph is rappresented as CSR format*/
typedef struct graph {
	double w;
	int n;
	int m;
	/*for node i, returns the start index of adj_list to read neighbours*/
	int *indexes;
	int *adj_list;
	double *weights;	
} t_graph;

/*Used to implement the Breadth First Search and to return to the calling function the updated values*/
typedef struct bfs {
	/*Array of flags - visited_nodes[i] == 1 if the node was visited during the search*/
	int *visited_nodes;
	/*Total number of visited nodes*/
	int  visited_nodes_num;
	/*Updated number of edges*/
	int updated_m;
} t_bfs;

t_graph* init_graph(int nodes, int edges, double max_weight);
void init_indexes_array(t_graph *g);
void init_adj_list(t_graph *g);
void generate_graph(t_graph *g, int d, int model);
void generate_weights(t_graph *g);
void generate_indexes(int *edges, t_graph *g);
void generate_adj_list(int *edges, t_graph *g);
void insert_edge(int index_from, int index_to, int node_to, t_graph *g);

t_bfs* init_bfs_struct(int n, int m);
void breadth_first_search(t_bfs *bfs_struct, t_graph *g);
int* update_edges(int *tmp_edges, t_bfs *bfs_struct, int nodes, int current_m);
int get_degree(t_graph *g, int node_index);

#endif
