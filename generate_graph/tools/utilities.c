#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <errno.h>

#include "utilities.h"
#include "../graphs/graphs.h"

#define MAX_CHAR 64

/*File extension and directory for graph files*/
const char *ext = ".graph";
const char *dir = "graphs/";
const char *path_file = "tools/sample.graph";

/* Converts from string to int 
 * Used to parse user input configs
 * */
int convert_string_to_int(char *s) {
	long i = strtol(s, NULL, 10);
	if (i == LONG_MAX || i == LONG_MIN) {
		fprintf(stderr, "Error converting with strtol\n");
		exit(EXIT_FAILURE);		
	}
	
	return (int)i;
}

/* Converts from string to double 
 * Used to parse user input configs
 * */
double convert_string_to_double(char *s) {
	double d;
	sscanf(s, "%lf", &d);
	return d;
}

/*Used to print the graph representation*/
void print_graph(t_graph *g) {
	int i = 0;
	int nodes = g->n;
	int edges = g->m;
	
	printf("Node: %d\nEdges: %d\nMax weight: %f\n", g->n, g->m, g->w);

	printf("Nodes indexes: ");	
	while (i<g->n) {
		printf("%d ", g->indexes[i]);
		if (i != nodes-1) {
			printf("- ");
		}
		i++;
	}
	i=0;
	printf("\nAdjacency List: ");	
	while (i<2*edges) {
		printf("%d ", g->adj_list[i]);
		if (i != 2*edges-1) {
			printf("- ");
		}
		i++;
	}
	i=0;
	printf("\nWeights List: ");	
	while (i<2*edges) {
		printf("%f ", g->weights[i]);
		if (i != 2*edges-1) {
			printf("- ");
		}
		i++;
	}
	printf("\n");
}


/*Writing the generated graph on a file*/
void write_graph_to_file(t_graph *g, int d){
	char path[MAX_CHAR];
	FILE *file;
	int i;
	int nodes = g->n;
	int edges = g->m;
	
	/*Creating the file name from the number of nodes and edges*/
	if (sprintf(path, "%s%c%d_%c%d_%c%d_%c%d%s", dir, 'n', g->n, 'm', g->m, 'w', (int)g->w, 'd', d, ext) < 0) {
		fprintf(stderr, "Error creating the file name - write_graph_to_file()\n");
		exit(EXIT_FAILURE);
	}
	
	if ((file = fopen(path,"a")) == NULL) {
		fprintf(stderr, "Error creating file - write_graph_to_file()\n");
		exit(EXIT_FAILURE);
	}
	
	/*Writing nodes indexes*/
	for (i=0; i<nodes-1; i++) {
		if (fprintf(file, "%d ", g->indexes[i]) < 0) {
			fprintf(stderr, "Error writing graph indexes - write_graph_to_file()\n");
			exit(EXIT_FAILURE);
		}
	}
	if (fprintf(file, "%d\n", g->indexes[nodes-1]) < 0) {
		fprintf(stderr, "Error writing graph indexes - write_graph_to_file()\n");
		exit(EXIT_FAILURE);
	}
	
	/*Writing the adj_list*/
	for (i=0; i<(2*edges)-1; i++) {
		if (fprintf(file, "%d ", g->adj_list[i]) < 0) {
			fprintf(stderr, "Error writing graph adj_list - write_graph_to_file()\n");
			exit(EXIT_FAILURE);
		}
	}
	if (fprintf(file, "%d\n", g->adj_list[(2*edges)-1]) < 0) {
		fprintf(stderr, "Error writing graph  adj_list - write_graph_to_file()\n");
		exit(EXIT_FAILURE);
	}
	
	/*Writing the weights*/
	for (i=0; i<(2*edges)-1; i++) {
		if (fprintf(file, "%lf ", g->weights[i]) < 0) {
			fprintf(stderr, "Error writing graph weights - write_graph_to_file()\n");
			exit(EXIT_FAILURE);
		}
	}
	if (fprintf(file, "%lf\n", g->weights[(2*edges)-1]) < 0) {
		fprintf(stderr, "Error writing graph  weights - write_graph_to_file()\n");
		exit(EXIT_FAILURE);
	}
	
	if (fclose(file) == EOF) {
		fprintf(stderr, "Error in fclose - write_graph_to_file()\n");
	}
}


void read_graph_from_file(int *tmp_edges, int edges) {
	FILE *file;
	int input;
	
	if ((file = fopen(path_file, "r")) == NULL) {
		fprintf(stderr, "Error reading file - read_graph_to_file()\n");
		exit(EXIT_FAILURE);
	}
	for (int i=0; i<edges*2; i++) {		
		if (fscanf(file, "%d", &input) == EOF) {
			fprintf(stderr, "Error reading graph file - read_graph_from_file() - edges\n");
			exit(EXIT_FAILURE);
		}
		tmp_edges[i] = input;
	}
	
	if (fclose(file) == EOF) {
		fprintf(stderr, "Error in fclose - read_graph_to_file()\n");
	}
}
