#ifndef __utilities_h
#define __utilities_h

#include "../graphs/graphs.h"

int convert_string_to_int(char *s);
double convert_string_to_double(char *s);
void print_graph(t_graph *g);
void write_graph_to_file(t_graph *g, int d);
void read_graph_from_file(int *tmp_edges, int edges);

#endif
