#include <stdio.h>
#include <stdlib.h>

#include "graphs/graphs.h"
#include "tools/utilities.h"
#include "tools/read_configs.h"

/*Max weight for edges*/
int w;
/*Number of nodes*/
int n;
/*Number of edges*/
int m;
/*Average degree of the graph or average degree of ring lattice (for Watts-Strogatz)*/
int d;
/*Struct representing the graph*/
t_graph *g;

int main(int argc, char** argv) {	
	double *configs = read_configs();
	int model;
	
	if (argc!=2) {
		printf("Error - please insert a graph model:\n\r-0 Random graph\n\r-1 Erdos-Renyi G(n,m)\n\r-2 Erdos-Renyi G(n,p)\n\r-3 Watts-Strogatz\n");
		exit(EXIT_FAILURE);
	}
	
	model = strtol(argv[1], NULL, 10);
	
	if (model<0 || model>4) {
		printf("Error - please insert a correct value for the graph model:\n\r-0 Random graph\n\r-1 Erdos-Renyi G(n,m)\n\r-2 Erdos-Renyi G(n,p)\n\r-3 Watts-Strogatz\n");
		exit(EXIT_FAILURE);
	}
	
	w = (int) configs[0];
	n = (int) configs[1];
	m = (int) configs[2];
	d = (int) configs[3];
	
	/*To generate a connected graph, the number of edges should be at least n-1*/
	if (w == 0 || n == 0 || m < n-1 || d == 0) {
		fprintf(stderr, "w, n and d must be > 0 and m >= n-1\n");
		exit(EXIT_FAILURE);
	}
	free(configs);
	
	g = init_graph(n, m, w);
	generate_graph(g, d, model);
	write_graph_to_file(g, d);
	//print_graph(g);
	
	exit(EXIT_SUCCESS);
}
