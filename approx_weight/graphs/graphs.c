#include <stdio.h>
#include <stdlib.h>

#include "graphs.h"

/*Creates and allocates the graph struct*/
t_graph* init_graph(int nodes, int edges, double max_weight) {
	t_graph *g;
	
	if ((g = malloc(sizeof(t_graph))) == NULL) {
		fprintf(stderr, "Error in malloc - init_graph()");
		exit(EXIT_FAILURE);
	}
	g->w = max_weight;
	g->n = nodes;
	g->m = edges;
	
	if ((g->indexes = malloc(sizeof(int)*nodes)) == NULL) {
		fprintf(stderr, "Error in malloc - init_graph() - indexes");
		exit(EXIT_FAILURE);
	}
	
	/*Each edge is represented twice in the array*/
	if ((g->adj_list = malloc(sizeof(int)*edges*2)) == NULL) {
		fprintf(stderr, "Error in malloc - init_graph() - adj_list");
		exit(EXIT_FAILURE);
	}
	
	/*Each edge is represented twice in the array*/
	if ((g->weights = malloc(sizeof(double)*edges*2)) == NULL) {
		fprintf(stderr, "Error in malloc - init_graph() - weights");
		exit(EXIT_FAILURE);
	}
	
	return g;
}

/*Reads the graph from the file and puts in the struct*/
void read_graph_from_file(t_graph *g, const char *file_name){
	FILE *file;
	int input;
	double weight;
	int i = 0;
	
	if ((file = fopen(file_name, "r")) == NULL) {
		fprintf(stderr, "Error opening graph file - read_graph_from_file()");
		exit(EXIT_FAILURE);
	}
	
	while (i<(g->n)) {		
		if (fscanf(file, "%d", &input) == EOF) {
			fprintf(stderr, "Error reading graph file - read_graph_from_file() - n");
			exit(EXIT_FAILURE);
		}
		g->indexes[i] = input;
		i++;
	}
	i=0;
	while (i<(g->m)*2) {
		if (fscanf(file, "%d", &input) == EOF) {
			fprintf(stderr, "Error reading graph file - read_graph_from_file() - m");
			exit(EXIT_FAILURE);
		}
		g->adj_list[i] = input;
		i++;
	}
	i=0;
	while (i<(g->m)*2) {
		if (fscanf(file, "%lf", &weight) == EOF) {
			fprintf(stderr, "Error reading graph file - read_graph_from_file() - w");
			exit(EXIT_FAILURE);
		}
		g->weights[i] = weight;
		i++;
	}	
	
	if (fclose(file) == EOF) {
		fprintf(stderr, "Error in fclose - read_graphs_from_file()");
	}
}

/*Returns the degree of the given node*/
int get_degree(t_graph *g, int node_index, int max_w) {
	int from, to;
	int degree = 0;
	
	from = g->indexes[node_index];
	if (node_index < g->n-1) {
		to = g->indexes[node_index+1];
	}
	else {
		to = 2*g->m;
	}
	
	for (int i=from; i<to; i++) {
		if (g->adj_list[i] != -1 && g->weights[i]<=max_w) {
			if (g->adj_list[i] != node_index) {
				degree++;
			}
			else {
				degree+=2;
			}
		}
	}
	
	return degree;
}

/*Returns the max weight in the graph*/
void set_max_weight(t_graph *g) {
	double max = 0.0;
	int tot_edges = 2*(g->m);
	
	for (int i=0; i<tot_edges; i++) {
		if (g->weights[i] > max) {
			max = g->weights[i];
		}
	}
	
	g->w = max;
}
