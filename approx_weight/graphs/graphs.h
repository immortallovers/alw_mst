#ifndef __graphs_h
#define __graphs_h

/*The graph is rappresented as CSR format*/
typedef struct graph {
	double w;
	int n;
	int m;
	/*for node i, returns the start index of adj_list to read neighbours*/
	int *indexes;
	int *adj_list;
	double *weights;	
} t_graph;

t_graph* init_graph(int nodes, int edges, double max_weight);
void read_graph_from_file(t_graph *g, const char *file_name);

int get_degree(t_graph *g, int node_index, int max_w);
void set_max_weight(t_graph *g);

#endif
