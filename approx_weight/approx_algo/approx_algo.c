#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <time.h>
#include <math.h>
#include <string.h>

#include "approx_algo.h"
#include "../graphs/graphs.h"
#include "../queue/queue.h"


/*Used to generate uniformly distributed random integers between 0 and num_nodes-1 included*/
int* random_node_generation(int num_nodes, int extractions) {
	/*Array of boolean values to avoid dulpicate extractions*/
	int check_for_duplicates[num_nodes];
	int extracted_num;
	int i;
	int *extracted_nodes;
	
	if ((extracted_nodes = malloc(sizeof(int)*extractions)) == NULL) {
		fprintf(stderr, "Error in malloc - random_node_generation()\n");
		exit(EXIT_FAILURE);
	}
	
	for (i=0; i<num_nodes; i++) {
		check_for_duplicates[i] = 0;
	}
	
	i=0;
	
	if (extractions < num_nodes) {			
		while(i<extractions) {
			extracted_num = rand()%num_nodes;
			/*Checking for duplicates*/
			if (!check_for_duplicates[extracted_num]) {
				extracted_nodes[i] = extracted_num;
				check_for_duplicates[extracted_num] = 1;
				i++;
			}
		}
	}
	else {
		/* We can't avoid duplicates because the required number of extractions 
		 * is bigger tha the number of nodes in the graph*/
		 while(i<extractions) {
			extracted_num = rand()%num_nodes;
			extracted_nodes[i] = extracted_num;
			i++;
		}
	}
	
	return extracted_nodes;
}


/*Returns the average degree of the graph*/
double get_average_degree(t_graph *g, int num_nodes, double epsilon, int C) {
	int extractions = C/epsilon;
	int i;
	int current_node;
	double avg_degree = 0;
	double current_degree;
	srand(time(NULL));
	
	/*Extracting C/epsilon nodes and checking their degree to pick the max one*/
	for (i=0; i<extractions; i++) {
		current_node = rand()%num_nodes;
		current_degree = get_degree(g, current_node, g->w);
		if (current_degree > avg_degree) {
			avg_degree = current_degree;
		}
	}
	
	return avg_degree;
}

/* Returns an array set to false 
 * used to keep track of the visited and open nodes and edges for the BFS
 * visited_nodes[i] == -1 -> closed node
 * visited_nodes[i] ==  0 -> open node
 * visited_nodes[i] ==  1 -> visited node
 * */
void init_visited_nodes(char *visited_nodes, int num_nodes) {	
	/*for (int i=0; i<num_nodes; i++) {
		visited_nodes[i] = -1;
	}*/
	memset(visited_nodes, -1, sizeof(char)*num_nodes);
}

/*Initializes the t_bfs_info struct*/
t_bfs_info* init_bfs_info(int num_nodes) {
	t_bfs_info *info;
	
	if ((info = malloc(sizeof(t_bfs_info))) == NULL) {
		fprintf(stderr, "Error in malloc - init_bfs_info() - t_bfs_info\n");
		exit(EXIT_FAILURE);
	}
	
	/*visited_nodes array*/
	if ((info->visited_nodes = malloc(sizeof(char)*num_nodes)) == NULL) {
		fprintf(stderr, "Error in malloc - init_bfs_info - visited_nodes()\n");
		exit(EXIT_FAILURE);
	}
	
	reset_bfs_info(info, num_nodes);
	
	return info;	
}

/*Deleting the bfs_info struct and freeing memory*/
void delete_bfs_info(t_bfs_info *info) {
	free(info->visited_nodes);
	free(info);
}

/*Resets the info for the following BFS - called once for every extracted node*/
void reset_bfs_info(t_bfs_info *info, int num_nodes) {
	init_visited_nodes(info->visited_nodes, num_nodes);
	
	info->visited_edges = 0;
	info->previous_visited_edges = 0;
	info->num_visited_nodes = 0;
	info->degree_condition = 0;
	info->initial_node_degree = 0;
}

/*Computing the first step of the BFS*/
void bfs_first_step(t_graph *g, t_bfs_info *info, t_queue *q, double max_w, int current_node) {
	int from, to;
	from = g->indexes[current_node];
	if (current_node < g->n-1) {
		to = g->indexes[current_node+1];
	}
	else {
		to = 2*g->m;
	}
	for (int j=from; j<to; j++) {
		if (g->adj_list[j] != -1) {
			if (info->visited_nodes[g->adj_list[j]]<0 && g->weights[j]<=max_w) {
				/*Non visited or non open valid node*/
				/*The node is now open*/
				info->visited_nodes[g->adj_list[j]] = 0;
				enqueue(q, g->adj_list[j]);
				/*If the node was closed, it was never seen before so this edge is new*/
				++info->visited_edges;
			}
		}
	}	
}

/*Generic BFS*/
double bfs_generic(t_graph *g, t_bfs_info *info, t_queue *q, double W, double average_degree, double max_w) {
	int exit = 0;
	int i;
	double beta_i = 0.0;
	int from, to;
	int current_node;
	/*There was at least one flip after tge BFS first step*/
	int num_flip_coin = 1;
	
	info->previous_visited_edges = info->visited_edges;
	
	
	while (!is_empty(q) && !exit) {
		current_node = pop(q);
		from = g->indexes[current_node];
		if (current_node < g->n-1) {
			to = g->indexes[current_node+1];
		}
		else {
			to = 2*g->m;
		}
		i = from;
		while (i<to && !exit) {
			if (g->adj_list[i] != -1) {
				if (info->visited_nodes[g->adj_list[i]]<0 && g->weights[i]<=max_w) {
					/*New distinct edge*/
					++info->visited_edges;
					enqueue(q, g->adj_list[i]);
					info->visited_nodes[g->adj_list[i]] = 0;
					if (2*info->previous_visited_edges == info->visited_edges) {
						/*The number of edges was doubled - we need to check the conditions*/
						if (!is_empty(q) || i<to-1) {
							/*The BFS is not completed yet*/
							if (flip_coin() && info->num_visited_nodes<W && !info->degree_condition) {
								num_flip_coin++;
								info->previous_visited_edges = info->visited_edges;
							}
							else {
								exit = 1;
							}
						}
						/*If the BFS is completed (is_empty(q)) the condition will be checked in the while loop*/
					}
				}
			}
			i++;
		}
		if (!exit) {
			/*We completed the visit of current_node*/
			info->visited_nodes[current_node] = 1;
			++info->num_visited_nodes;
			if (get_degree(g, current_node, max_w) > average_degree) {
				info->degree_condition = 1;
			}
		}
	}
	if (is_empty(q) && !exit) {
		/*We completed the BFS of the graph*/
		//printf("Completed the BFS for G(%lf)\n", max_w);
		beta_i = ((info->initial_node_degree*pow(2, num_flip_coin))/info->visited_edges);
	}
	return beta_i;
}


/*Flipping the coin for the BFS*/
int flip_coin() {
	int coin = rand()%2;
	//printf("COIN %d\n", coin);
	return coin;
}


/* ---------------------------------------------------------------------
 * 			Estimating the number of connected components
 * 			  max_w is the max weight considered (G(l))
 * ---------------------------------------------------------------------*/
double estimate_num_c(t_graph *g, double epsilon, double W, double average_degree, double max_w) {
	/*The estimated number of connected components*/
	double num_c = 0.0;
	int *extracted_nodes;
	/*Number of extractions*/
	int r;
	double beta_i;
	double beta_sum = 0.0;
	t_queue *q;
	/*Struct containg useful information about the state of the current BFS*/
	t_bfs_info *info;
	int current_node;
	
	srand(time(NULL));
	
	/*r = O(1/epsilon²)*/
	r = (int)ceil(1/(pow(epsilon, 2)));
	//printf("R %d - W %lf\n", r, W);
	/*Choosing r nodes*/
	extracted_nodes = random_node_generation(g->n, r);
	
	/* Printing the extracted nodes
	for(int j=0; j<r; j++) {
		printf("Extracted Nodes: %d\n", extracted_nodes[j]);
	}
	*/
	
	/*t_bfs_info struct initialization*/
	info = init_bfs_info(g->n);
	
	/*queue initialization*/
	q = init_queue();
	
	/*BFS for each extracted node*/
	for (int i=0; i<r; i++) {
		beta_i = 0.0;
		/*Resetting the t_bfs_info struct*/
		reset_bfs_info(info, g->n);
		current_node = extracted_nodes[i]; 
		info->visited_nodes[current_node] = 0;
		
		/* ----------------------------
		 * 	  First step of the BFS
		 * ----------------------------*/
		if ((info->initial_node_degree = get_degree(g, current_node, max_w)) > average_degree) {
			/*Abort current node BFS*/
			/*Resetting the queue*/
			while(pop(q) != -1);
			continue;
		}
		bfs_first_step(g, info, q, max_w, current_node);
		if (info->initial_node_degree > average_degree) {
			info->degree_condition = 1;
		}
		
		/*We consider a node visited when it's pull out of the queue and all his edges are visisted - otherwise is an open node*/
		info->visited_nodes[current_node] = 1;
		info->num_visited_nodes = 1;
		
		
		/* ----------------------------
		 * 	Following steps of the BFS
		 * ----------------------------*/
		if (flip_coin() && info->num_visited_nodes<W && !info->degree_condition) {
			if (!is_empty(q)) {
				beta_i = bfs_generic(g, info, q, W, average_degree, max_w);
			}
			else {
				/*mu_i (visited_edges) == 0 -> beta_i = 2*/
				beta_i = 2;
			}
		}
		
		beta_sum += beta_i;
		
		/*Resetting the queue*/
		while(pop(q) != -1);
	}
	
	/* ----------------------------
	 * 	     Computing num_c
	 * ----------------------------*/
	num_c = (g->n * beta_sum)/(2*r);
	
	delete_bfs_info(info);
	free(extracted_nodes);
	free(q);
	
	
	//printf("Estimated number of connected components for G(%d) = %lf\n", (int)max_w, num_c);
	
	return num_c;
}

/* ---------------------------------------------------------------------
 * 				Estimating the weight of the MST
 * ---------------------------------------------------------------------*/
double approx_weight(t_graph *g, double epsilon, double max_w, double W, double average_degree) {
	double approx_weight;
	double tot_c = 0.0;
	double tmp;
	
	//printf("Average degree %lf\n", average_degree);
	
	/* Computing the estimated number of connected components 
	 * for all the subgraphs with weights from 1 to w-1 */
	for (int i=1; i<max_w; i++) {
		tmp = estimate_num_c(g, epsilon, W, average_degree, i);
		tot_c += tmp;
		//printf("C= %lf\n", tmp);
	}
	
	approx_weight = g->n - max_w + tot_c;
	return approx_weight;
}
