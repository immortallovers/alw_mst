#ifndef __approx_algo_h
#define __approx_algo_h

#include "../graphs/graphs.h"
#include "../queue/queue.h"

typedef struct bfs_info {
	/* Array of flags - visited_node[i] == -1 ->closed node, 
	 * visited_node[i] == 0 ->open node, visited_node[i] == 1 ->visited node,*/
	char *visited_nodes;
	/*Number of distinct edges visited*/
	int visited_edges;
	/*Nmuber of distinct edges visited before the following step of the BFS (used to stop when the number doubles)*/
	int previous_visited_edges;
	/*Number of visited nodes*/
	int num_visited_nodes;
	/*Set to 1 if one of the visited nodes has degree>average_degree - 0 otherwise*/
	int degree_condition;
	/*Degree of the first node visited*/
	int initial_node_degree;
} t_bfs_info;

double approx_weight(t_graph *g, double epsilon, double max_w, double W, double average_degree);
double estimate_num_c(t_graph *g, double epsilon, double W, double average_degree, double max_w);

int* random_node_generation(int num_nodes, int extractions);
double get_average_degree(t_graph *g, int num_nodes, double epsilon, int C);
t_bfs_info* init_bfs_info(int num_nodes);
void delete_bfs_info(t_bfs_info *info);
void init_visited_nodes(char *visited_nodes, int r);
void reset_bfs_info(t_bfs_info *info, int num_nodes);
void bfs_first_step(t_graph *g, t_bfs_info *info, t_queue *q, double max_w, int current_node);
double bfs_generic(t_graph *g, t_bfs_info *info, t_queue *q, double W, double average_degree, double max_w);
int flip_coin();

#endif
