#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "graphs/graphs.h"
#include "tools/utilities.h"
#include "tools/read_configs.h"
#include "approx_algo/approx_algo.h"


/*Number of configs to be read*/
#define CONFIGS_NUMBER 3
/*Large costant used to compute the graph average degree estimation*/
#define C 123

const char *path_graph = "graphs/sample.graph";

/*0 < epsilon <1/2*/
double epsilon;
/*Max weight for edges*/
int w;
/*Number of nodes*/
int n;
/*Number of edges*/
int m;
/*Threshold value for the approximation algorithm*/
double W;
/*Struct representing the graph*/
t_graph *g;

int main(void) {
	double *configs = read_configs();
	double average_degree;
	double weight;
	struct timeval start, end;
	
	/*Reading configs*/
	if ((epsilon=configs[0]) < 0 || epsilon >= 0.5) {
		fprintf(stderr, "Epsilon must be 0 < epsilon < 1/2\n");
		exit(EXIT_FAILURE);
	}
	w = (int) configs[1];
	n = (int) configs[2];
	m = (int) configs[3];
	
	if (w == 0 || n == 0 || m < n-1) {
		fprintf(stderr, "Must be w > 2, n > 0 and m >= n-1\n");
		exit(EXIT_FAILURE);
	}
	free(configs);
	
	/*Reading the graph*/
	g = init_graph(n, m, w);
	read_graph_from_file(g, path_graph);
	set_max_weight(g);
	//printf("Max weight %lf\n", g->w);
	//print_graph(g);
	
	/*Computing the approximation algorithm*/
	W = 4.0*g->w/epsilon;
	average_degree = get_average_degree(g, n, epsilon, C);
	gettimeofday(&start, NULL);
	weight = approx_weight(g, epsilon, (int)g->w, W, average_degree);
	gettimeofday(&end, NULL);
	//printf("The approximated weight of the MST is %lf\n", weight);
	//printf("Running Time %ld us\n", ((end.tv_sec*1000000 + end.tv_usec) - (start.tv_sec*1000000 + start.tv_usec)));
	printf("%lf, %ld\n", weight, ((end.tv_sec*1000000 + end.tv_usec) - (start.tv_sec*1000000 + start.tv_usec)));
	
	exit(EXIT_SUCCESS);
}
