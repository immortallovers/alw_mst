#ifndef __queue_h
#define __queue_h

/*Represents and element of the queue*/
typedef struct node {
	int node_index;
	struct t_node* next;
} t_node;


/*Represents the queue*/
typedef struct queue {
	t_node *first;
	t_node *last;
} t_queue;

t_queue* init_queue();
int is_empty(t_queue *q);
void enqueue(t_queue *q, int node_index);
int get_first_node(t_queue *q);
int pop(t_queue *q);

#endif
