#include <stdio.h>
#include <stdlib.h>

#include "queue.h"

/*Initializing the queue*/
t_queue* init_queue() {
	t_queue *q;
	
	if ((q = malloc(sizeof(t_queue))) == NULL) {
		fprintf(stderr, "Error in malloc - init_queue()\n");
		exit(EXIT_FAILURE);
	}
	
	q->first = NULL;
	q->last = NULL;
	
	return q;
}

/*Returns 1 if the queue is esmpty, 0 otherwise*/
int is_empty(t_queue *q) {
	if (q->first == NULL) {
		return 1;
	}
	return 0;
}


/*Add an element to the end of the queue*/
void enqueue(t_queue *q, int node_index) {
	t_node *new_elem;
	
	if ((new_elem = malloc(sizeof(t_node))) == NULL) {
		fprintf(stderr, "Error in malloc - enqueue()\n");
		exit(EXIT_FAILURE);
	}
	new_elem->next = NULL;
	new_elem->node_index = node_index;
	
	if (is_empty(q)) {
		/*This is the first node*/
		q->first = q->last = new_elem;
	}
	else {
		q->last->next = new_elem;
		q->last = new_elem;
	}
}

/*Returns the node_index of the first element in the queue*/
int get_first_node(t_queue *q) {
	int  node_index;
	
	if(is_empty(q)) {
		/*The queue is empty*/
		//printf("The queue is empty - no element to return\n");
		return -1;
	}
	else {
		node_index = q->first->node_index;
	}
	
	return node_index;
}

/*Returns the node_index of the first element in the queue after deleting it*/
int pop(t_queue *q) {
	int node_index;
	t_node *deleted_node;
	
	node_index = get_first_node(q);
	if (node_index != -1) {
		/*There is at least one element in the queue*/
		deleted_node = q->first;
		q->first = q->first->next;
		free(deleted_node);
	}
	else {
		return -1;
	}

	return node_index;
}
